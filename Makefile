# https://docs.traefik.io/user-guide/kubernetes/
# https://github.com/containous/traefik/blob/master/examples/k8s/traefik-rbac.yaml

create: create-rbac-permissions create-controller create-webui-service

create-rbac-permissions:
	kubectl apply -f traefik-rbac.yaml

create-controller:
	kubectl apply -f traefik-ingress-controller.yaml

create-webui-service:
	kubectl apply -f traefik-webui.yaml

kube-system-pods:
	kubectl -n kube-system get pods


# Delete custom helm chart artifacts
get-secrets:
	kubectl get secrets --all-namespaces=true

delete-secret:
	kubectl delete secret secret-name


# Allow tiller RBAC permissions
# https://github.com/kubernetes/helm/issues/3130
allow-tiller: create-serviceaccount create-clusterrolebinding patch-deploy finalize

create-serviceaccount:
	kubectl create serviceaccount --namespace kube-system tiller

create-clusterrolebinding:
	kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

patch-deploy:
	kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'

finalize:
	helm list && helm repo update


# Tiller RBAC solution 2
# https://medium.com/@amimahloof/how-to-setup-helm-and-tiller-with-rbac-and-namespaces-34bf27f7d3c3